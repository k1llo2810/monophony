��    N      �  k   �      �     �     �     �     �     �     �  	   �          	  	     
        %     6     >     S     l     s     �     �     �     �     �     �  	   �     �               #  -   5  -   c     �     �     �     �  	   �     �  
   �     �     �     �     	  	   !	  
   +	     6	     F	     I	     O	     b	     g	     p	     �	     �	     �	  
   �	     �	     �	     �	     �	     
  	   
     
     
     4
     A
  
   M
     X
     `
     f
     s
  
   �
     �
     �
     �
     �
     �
     �
     �
  9  �
          ;     V     [     m     y     �     �     �  
   �     �     �     �      �        	   8     B  .   `     �  	   �     �     �     �     �  %   �  (        C     J  <   b  9   �     �     �     �          .     >     Z     j     �     �  "   �     �     �     �     �     �  !   �          '  !   8     Z     h     x  
   �     �  +   �  !   �     �  
              /     A     Z     j     }     �     �     �     �     �     �     �     �     �             .   8         $   C            A       #   7   K   4   G                     +   8       D      "       %   6           <       H   :              J          L       E      )                1   F   @      >   '       0   2   N   ;   ?   *   M         =       &                     
      !              3                   ,             -   (      B       .          /   	      5       9       I            A URL is required. A name is required. About About Monophony Add Add to library Add to... Added Albums All Songs All Videos Artist Not Found Artists As Editable Playlist As Synchronized Playlist Cancel Community Playlists Could not Import Playlist Could not Rename Create Delete Deleted "{playlist_name}" Download Duplicate Enter Playlist Name... Enter Playlist URL... External External Playlist Failed to retrieve playlist data from server. Find songs to play using the search bar above Go back Import Import Playlist Import playlist... Import... Loading Library... Loading... Local Playlist More More actions New Playlist Name... Next song No Results Normal Playback Ok Other Parsing Results... Play Play All Playlist already exists Previous song Primary Menu Queue Radio Mode Recently Played Remove From Downloads Remove From Playlist Remove From Queue Rename Rename... Repeat Song Search for Content... Searching... Show Artist Show Queue Shuffle Songs Synchronized Toggle pause Top Result Undo Videos View Artist Volume Your Library is Empty Your Playlists translator-credits Project-Id-Version: 
PO-Revision-Date: 
Last-Translator: Jürgen Benvenuti <gastornis@posteo.org>
Language-Team: German <gnome-de@gnome.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.3.1
 Eine Adresse ist erforderlich. Ein Name ist erforderlich. Info Info zu Monophony Hinzufügen Zur Bibliothek hinzufügen Hinzufügen zu … Hinzugefügt Alben Alle Titel Alle Videos Interpret nicht gefunden Interpreten Als bearbeitbare Wiedergabeliste Als abgeglichene Wiedergabeliste Abbrechen Gemeinschaftswiedergabelisten Wiedergabeliste konnte nicht importiert werden Umbenennen nicht möglich Erstellen Löschen »{playlist_name}« gelöscht Herunterladen Duplizieren Name der Wiedergabeliste eingeben … Adresse der Wiedergabeliste eingeben … Extern Externe Wiedergabeliste Abrufen der Wiedergabelistendaten vom Server fehlgeschlagen. Suchen Sie in der Leiste oben nach Titeln zum Wiedergeben Zurückgehen Importieren Wiedergabeliste importieren Wiedergabeliste importieren … Importieren … Bibliothek wird geladen … Ladevorgang … Lokale Wiedergabeliste Mehr Weitere Aktionen Name der neuen Wiedergabeliste … Nächster Titel Keine Ergebnisse Normale Wiedergabe OK Weitere Ergebnisse werden verarbeitet … Wiedergeben Alle wiedergeben Wiedergabeliste existiert bereits Voriger Titel Primäres Menü Warteschlange Radiomodus Zuletzt wiedergegeben Aus den heruntergeladenen Dateien entfernen Aus der Wiedergabeliste entfernen Aus der Warteschlange entfernen Umbenennen Umbenennen … Titel wiederholen Nach Inhalten suchen … Suchvorgang … Interpret anzeigen Warteschlange anzeigen Durchmischen Titel Abgeglichen Wiedergabe/Pause Bestes Ergebnis Rückgängig Videos Interpret anzeigen Lautstärke Ihre Bibliothek ist leer Eigene Wiedergabelisten Jürgen Benvenuti <gastornis@posteo.org>, 2023 