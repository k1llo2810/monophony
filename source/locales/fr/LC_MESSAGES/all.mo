��    N      �  k   �      �     �     �     �     �     �     �  	   �          	  	     
        %     6     >     S     l     s     �     �     �     �     �     �  	   �     �               #  -   5  -   c     �     �     �     �  	   �     �  
   �     �     �     �     	  	   !	  
   +	     6	     F	     I	     O	     b	     g	     p	     �	     �	     �	  
   �	     �	     �	     �	     �	     
  	   
     
     
     4
     A
  
   M
     X
     `
     f
     s
  
   �
     �
     �
     �
     �
     �
     �
     �
    �
     �       	   '     1     H     P     l     z     �     �     �     �     �     �     �     �     �  #        <     S  	   Z  !   d     �  	   �     �     �     �     �  J   �  E   =     �     �     �     �     �  !   �     �     �               &     A     O     _     o     r     x     �  
   �     �     �     �     �  
   �     �          +     B     a     j     v     �     �     �     �  	   �     �     �                #     +     3     D     K     h     v         $   C            A       #   7   K   4   G                     +   8       D      "       %   6           <       H   :              J          L       E      )                1   F   @      >   '       0   2   N   ;   ?   *   M         =       &                     
      !              3                   ,             -   (      B       .          /   	      5       9       I            A URL is required. A name is required. About About Monophony Add Add to library Add to... Added Albums All Songs All Videos Artist Not Found Artists As Editable Playlist As Synchronized Playlist Cancel Community Playlists Could not Import Playlist Could not Rename Create Delete Deleted "{playlist_name}" Download Duplicate Enter Playlist Name... Enter Playlist URL... External External Playlist Failed to retrieve playlist data from server. Find songs to play using the search bar above Go back Import Import Playlist Import playlist... Import... Loading Library... Loading... Local Playlist More More actions New Playlist Name... Next song No Results Normal Playback Ok Other Parsing Results... Play Play All Playlist already exists Previous song Primary Menu Queue Radio Mode Recently Played Remove From Downloads Remove From Playlist Remove From Queue Rename Rename... Repeat Song Search for Content... Searching... Show Artist Show Queue Shuffle Songs Synchronized Toggle pause Top Result Undo Videos View Artist Volume Your Library is Empty Your Playlists translator-credits Project-Id-Version: 
PO-Revision-Date: 
Last-Translator: Irénée Thirion <irenee.thirion@e.email>
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 3.4
 Une URL est requise. Un nom est requis. À propos À propos de Monophony Ajouter Ajouter à la bibliothèque Ajouter à… Ajouté Albums Tous les titres Toutes les vidéos Artiste non trouvé Artistes Playlist modifiable Playlist synchronisée Annuler Playlists de la communauté Impossible d’importer la playlist Impossible de renommer Créer Supprimer Supprimer « {playlist_name} » Télécharger Dupliquer Entrez un nom de playlist… Entrez une URL de playlist… Externe Playlist externe Échec de la récupération des données de la playlist depuis le serveur. Recherchez des titres à jouer depuis la barre de recherche ci-dessus Retour Importer Importer une playlist Importer une playlist… Importer… Chargement de la bibliothèque… Chargement… Playlists locales Plus Plus d’actions Nouveau nom de playlist… Titre suivant Aucun résultat Lecture normale OK Autre Analyse des résultats… Jouer Jouer tout La playlist existe déjà Titre précédent Menu principal File d’attente Mode radio Joué récemment Supprimer des téléchargements Retirer de la playlist Retirer de la file d’attente Renommer Renommer… Répéter le titre Rechercher du contenu… Rechercher… Afficher l’artiste Afficher la file d’attente Mélanger Titres Synchronisé Mettre en pause Résultat en tête Annuler Vidéos Voir l’artiste Volume Votre bibliothèque est vide Vos playlists Irénée Thirion 